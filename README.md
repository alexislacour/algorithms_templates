# Algorithms Templates

These are some code templates of algorithms and data structures, including:

- bisect.cpp: binary search in half-closed-half-open index interval [a, b).
- quicksort.cpp: quick sort with elements equal to pivot aggregated, pivot picked from triple candidates(first, middle and last ones) and recursion optimized.

