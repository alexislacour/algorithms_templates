#include <bits/stdc++.h>
using namespace std;

// return the index of which is the first element greater than
//   or equal to num in arr[first, last)
int bisect(int arr[], int first, int last, int num) {
  while (first < last) {
    int mid = first + ((last - first) >> 1);
    if (arr[mid] < num) first = mid + 1;
    else last = mid;
  }
  return first;
}

int main() {
  int arr[] = {-1, 1, 1, 2, 2, 3, 4, 4, 6, 7, 7, 8};
  int num;
  while (~scanf("%d", &num))
    printf("idx is %d\n", bisect(arr, 1, 12, num));
  return 0;
}

