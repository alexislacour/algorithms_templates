#include <bits/stdc++.h>
using namespace std;

inline void read(int& num) {
  num = 0;
  char c = getchar();
  while (!isdigit(c)) c = getchar();
  while (isdigit(c)) {
    num = (num << 3) + (num << 1) + (c - '0');
    c = getchar();
  }
}

const int maxn = 100000 + 3;
int n, arr[maxn];

// |****|****|*****|
// |smaller than pivot|equal to pivot|larger than pivot|
// lpivot: the last element smaller than pivot
// rpivot: the first element larger than pivot
void my_partition(int first, int last, int& lpivot, int& rpivot) {
  lpivot = first - 1;
  rpivot = first;
  for (int i = first; i < last; i++) {
    if (arr[i] < arr[last]) {
      swap(arr[i], arr[rpivot]);
      swap(arr[++lpivot], arr[rpivot++]);
    }
    else if (arr[i] == arr[last]) swap(arr[i], arr[rpivot++]);
  }
  swap(arr[last], arr[rpivot++]);
}

void my_quicksort(int first, int last) {
  int mid = first + ((last - first) >> 1);
  if (arr[mid] < arr[first]) swap(arr[mid], arr[first]);
  if (arr[mid] > arr[last]) swap(arr[mid], arr[last]);
  if (arr[first] > arr[mid]) swap(arr[first], arr[mid]);
  swap(arr[mid], arr[last]);
  while (first < last) {
    int lpivot, rpivot;
    my_partition(first, last, lpivot, rpivot);
    if (lpivot > first) my_quicksort(first, lpivot);
    if (rpivot < last) first = rpivot;
    else return;
  }
}

int main() {
  //freopen("input.txt", "r", stdin);
  while (~scanf("%d", &n)) {
    for (int i = 1; i <= n; i++) read(arr[i]);
    my_quicksort(1, n);
    for (int i = 1; i <= n; i++) {
      if (i > 1) putchar(' ');
      printf("%d", arr[i]);
    }
    putchar('\n');
  }
  return 0;
}
